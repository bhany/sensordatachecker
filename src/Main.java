import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	static BufferedReader br = null;
    static String line = "";
    static String cvsSplitBy = ",";
    static int data_total = 0;

    public static void main(String[] args) {
    	int error = 0;
//    	
//    	String csvFile = "C:/Users/Joe/Downloads/sensorcsv/1-4 40plus.csv";
//    	getNullCount(csvFile);
//    	error += getNoPairCount(csvFile);
//    	error += orderCheck(csvFile);
//
//    	csvFile = "C:/Users/Joe/Downloads/sensorcsv/1-4 90plus.csv";
//    	getNullCount(csvFile);
//    	error += getNoPairCount(csvFile);
//    	error += orderCheck(csvFile);
//
//    	csvFile = "C:/Users/Joe/Downloads/sensorcsv/1-4 140plus.csv";
//    	getNullCount(csvFile);
//    	error += getNoPairCount(csvFile);
//    	error += orderCheck(csvFile);
    	

    	String csvFile = "C:/Users/Joe/Downloads/sensorcsv/1-6 160.csv";
    	getNullCount(csvFile);
    	error += getNoPairCount(csvFile);
    	error += orderCheck(csvFile);
    	
		System.out.println("out of total " + data_total + " rows, " + error + " rows are defective and should be disregarded (" + (double)error/data_total + "%)" );

    }
    
    public static int orderCheck(String csvFile) {
    	String tempTime = "";
    	int count = 0;
    	
    	try {
            br = new BufferedReader(new FileReader(csvFile));
            line = br.readLine();
            		
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] row = line.split(cvsSplitBy);
                
                if (tempTime.equals("")) {
                    tempTime = row[12];
                    continue;
                } else {
                	if (Long.valueOf(tempTime) > Long.valueOf(row[12])) {
                		count++;
                		System.out.println(Long.valueOf(tempTime) + " > " + Long.valueOf(row[12]) + " ?? occurs at " + row[0] );

                	}
                	
                	tempTime = row[12];
                }
            }
            
            System.out.println(count + " many data are out of order!");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
        }
    	
        return count;
    }
    
    public static int getNoPairCount(String csvFile) {
    	boolean accNoPair = false; 
    	boolean gyrNoPair = false; 

        int acc_count = 0;
        int gyro_count = 0;
        
        try {
            br = new BufferedReader(new FileReader(csvFile));
            
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] row = line.split(cvsSplitBy);
                
                if(accNoPair && (row[6].equals("NULL") && row[7].equals("NULL") && row[8].equals("NULL"))) {
                	accNoPair = false;
                	continue;
                } else if (accNoPair) {
                	accNoPair = false;
                	acc_count++;
                } else 
                	accNoPair = false;
                
            	if(gyrNoPair && (row[3].equals("NULL") && row[4].equals("NULL") && row[5].equals("NULL"))) {
                	gyrNoPair = false;
            		continue;
            	} else if (gyrNoPair) {
                	gyro_count++;
                	gyrNoPair = false;
                } else 
                	gyrNoPair = false;
                
                if (row[3].equals("NULL") && row[4].equals("NULL") && row[5].equals("NULL")) {
                	accNoPair = true;
                	continue;
                } else if (row[6].equals("NULL") && row[7].equals("NULL") && row[8].equals("NULL")) {
                	gyrNoPair = true;
                	continue;
                }
            }
            
            System.out.println(acc_count + " number of acc data does not have corresponding gyro data");
            System.out.println(gyro_count + " number of gyro data does not have corresponding acc data");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return acc_count + gyro_count;
    }
    
    public static int getNullCount(String csvFile) {
        int acc_count = 0;
        int gyro_count = 0;
        int count = 0;
        
        try {
            br = new BufferedReader(new FileReader(csvFile));
            
            
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] row = line.split(cvsSplitBy);

                if (row[3].equals("NULL") && row[4].equals("NULL") && row[5].equals("NULL")) {
//                    System.out.println("row " + row[0] + "'s acc data is NULL");
                    acc_count++;
                }
                
                if (row[6].equals("NULL") && row[7].equals("NULL") && row[8].equals("NULL")) {
//                    System.out.println("row " + row[0] + "'s gyro data is NULL");
                    gyro_count++;
                }
                
                count++;
            }
            
            System.out.println("Total number of rows is " + count);
            System.out.println(acc_count + " number of rows do not have acc data");
            System.out.println(gyro_count + " number of rows do not have gyro data");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        data_total += count; 
        return acc_count + gyro_count;
    }

}